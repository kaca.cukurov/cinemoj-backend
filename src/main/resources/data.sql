 -- GENRES
INSERT INTO genre VALUES (100,'Drama','Drama');
INSERT INTO genre VALUES (101,'Action','Akcija');
INSERT INTO genre VALUES (102,'Comedy','Komedija');
INSERT INTO genre VALUES (103,'Horror','Horor');
INSERT INTO genre VALUES (104,'Adventure','Avantura');
INSERT INTO genre VALUES (105,'Animated','Animirani');
INSERT INTO genre VALUES (106,'Documentary','Dokumentarac');
INSERT INTO genre VALUES (107,'Thriller','Triler');
INSERT INTO genre VALUES (108,'Family','Porodican');
INSERT INTO genre VALUES (109,'Crime','Krimi');
INSERT INTO genre VALUES (110,'Romance','Romanticni');
INSERT INTO genre VALUES (111,'Sci-Fi','Naucna fantastika');
INSERT INTO genre VALUES (112,'Fantasy','Fantazija');


 -- MOVIES
INSERT INTO movie (movie_id, description, description_serbian, duration, name, name_serbian, rating, soon, year, picture_url) VALUES (100, 'Anne Hathaway and Rebel Wilson star as female scam artists, one low rent and the other high class, who team up to take down the men who have wronged them.', 'Rimejk komedije iz 1988. “Prljavi, pokvareni prevaranti”, u kojoj se dve prefrigane prevarantkinje nadmeću ne bi li se rešilo koja od njih ostaje u gradu, a koja je gubitnik i odlazi “sa tržišta”.', 93, 'The Hustle', 'Prevaranti', 2, false, 2019, 'https://m.media-amazon.com/images/M/MV5BMTc3MDcyNzE5N15BMl5BanBnXkFtZTgwNzE2MDE0NzM@._V1_.jpg');
INSERT INTO movie (movie_id, description, description_serbian, duration, name, name_serbian, rating, soon, year, picture_url) VALUES (101, 'In a world where people collect Pokémon to do battle, a boy comes across an intelligent talking Pikachu who seeks to be a detective.', 'Pokemon groznica i dalje trese svet i neće tako brzo prestati, jer stiže prva živa Pokemon avantura sa popularnim Ryanom Reynoldsom u ulozi legendarnog Pikachua!', 104, 'Pokémon Detective Pikachu', 'Pokemon detektiv Pikachu', 3, false, 2019, 'https://m.media-amazon.com/images/M/MV5BNDU4Mzc3NzE5NV5BMl5BanBnXkFtZTgwMzE1NzI1NzM@._V1_.jpg');
INSERT INTO movie (movie_id, description, description_serbian, duration, name, name_serbian, rating, soon, year, picture_url) VALUES (102, 'A kind-hearted street urchin and a power-hungry Grand Vizier vie for a magic lamp that has the power to make their deepest wishes come true.', '“ALADIN” je uzbudljiva priča o šarmantnom mladiću koji većinu života provodi na ulici, Aladinu, hrabroj i odlučnoj princezi Jasmin i Duhu iz lampe koji može da bude glavni u određivanju njihove sudbine. ', 128, 'Aladdin', 'Aladin', 4, false, 2019, 'https://m.media-amazon.com/images/M/MV5BMjQ2ODIyMjY4MF5BMl5BanBnXkFtZTgwNzY4ODI2NzM@._V1_.jpg');
INSERT INTO movie (movie_id, description, description_serbian, duration, name, name_serbian, rating, soon, year, picture_url) VALUES (103, 'Super-assassin John Wick is on the run after killing a member of the international assassins guild, and with a $14 million price tag on his head - he is the target of hit men and women everywhere.', 'Halle Berry se pridružuje Keanu Reevesu u novom nastavku Johna Wicka i upravo zato se još više veselimo trećem nastavku.', '130', 'John Wick: Chapter 3 - Parabellum', 'Džon Vik 3 - Parabelum', 4, false, 2019, 'https://images-na.ssl-images-amazon.com/images/I/7177WCHzlgL._SY679_.jpg');
INSERT INTO movie (movie_id, description, description_serbian, duration, name, name_serbian, rating, soon, year, picture_url) VALUES (104, 'Journalist Fred Flarsky reunites with his childhood crush, Charlotte Field, now one of the most influential women in the world. As she prepares to make a run for the Presidency, Charlotte hires Fred as her speechwriter and sparks fly.', 'Od kreatora urnebesnih komedija: Komšije iz pakla i Zalomilo se!', 125, 'Long shot', 'Zavedi me ako mozes', 4, false, 2019, 'https://images-na.ssl-images-amazon.com/images/I/91sYRf2E8-L._SY679_.jpg');
INSERT INTO movie (movie_id, description, description_serbian, duration, name, name_serbian, rating, soon, year, picture_url) VALUES (105, 'After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to undo Thanos actions and restore order to the universe.', 'Četvrti film o najomiljenijim Osvetnicima kulminacija je 22 povezana filma, koji će omogućiti publici da budu svedoci prelomne tačke ovog epskog putovanja. Naši heroji moraće da podnesu veliku žrtvu ukoliko žele da očuvaju ovu krhku stvarnost.', 181, 'Avengers:Endgame', 'Osvetnici: Kraj igre', 4, false, 2019, 'https://images-na.ssl-images-amazon.com/images/I/71niXI3lxlL._SY606_.jpg');
INSERT INTO movie (movie_id, description, description_serbian, duration, name, name_serbian, rating, soon, year, picture_url) VALUES (106, 'The British monarchs (Dame Julie Walters) favorite dog gets lost from the palace and finds himself at a dog fight club. He then begins his long journey to find his way back home.', 'Animirani film o omiljenoj vrsti pasa britanske kraljice Elizabete, velškim korgijima, od kojih se još od detinjstva ne odvaja.', 92, 'The Queen’s Corgi', 'Korgi: Kraljevski pas velikog srca ', 2, false, 2019, 'https://m.media-amazon.com/images/M/MV5BZmQzNzZkZmEtYzhjMS00Y2RjLWE3MmYtZmU2MjlkMDU2ODRhXkEyXkFqcGdeQXVyMjQ3NzUxOTM@._V1_.jpg');
INSERT INTO movie (movie_id, description, description_serbian, duration, name, name_serbian, rating, soon, year, picture_url) VALUES (107, 'The crypto-zoological agency Monarch faces off against a battery of god-sized monsters, including the mighty Godzilla, who collides with Mothra, Rodan, and his ultimate nemesis, the three-headed King Ghidorah.', 'Kripto-zoološka agencija Monarch se suočava sa grupom čudovišta veličine boga, uključujući moćnog Godzilu, koja se sudara s Mothrom, Rodanom i njegovim krajnjim neprijateljem, trokrakim kraljem Gidorahom.', 131, 'Godzilla: King of the Monsters', 'Godzila 2: Kralj čudovišta', null, true, 2019, 'https://m.media-amazon.com/images/M/MV5BOGFjYWNkMTMtMTg1ZC00Y2I4LTg0ZTYtN2ZlMzI4MGQwNzg4XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg');
INSERT INTO movie (movie_id, description, description_serbian, duration, name, name_serbian, rating, soon, year, picture_url) VALUES (108, 'Continuing the story of Max and his pet friends, following their secret lives after their owners leave them for work or school each day.', 'Deseti animirani dugometražni film iz studija Illumination “Tajne avanture kućnih ljubimaca 2” jedan je od najočekivanijih filmova ove godine. U pitanju je nastavak blokbaster komedije iz 2016. godine.', 86, 'The Secret Life of Pets 2', 'Tajne avanture kućnih ljubimaca 2', 3, true, 2019, 'https://m.media-amazon.com/images/M/MV5BMTA2NzM0MjA0MTJeQTJeQWpwZ15BbWU4MDk1MzYwNzYz._V1_.jpg');
INSERT INTO movie (movie_id, description, description_serbian, duration, name, name_serbian, rating, soon, year, picture_url) VALUES (109, 'The Men in Black have always protected the Earth from the scum of the universe. In this new adventure, they tackle their biggest threat to date: a mole in the Men in Black organization.', 'Ljudi u crnom su uvek štitili zemlju od svemirskih neprijatelja. U najnovijoj avanturi neprijatelj se nalazi sakriven među njima samima.', 115, 'Men in Black: International', 'Ljudi u crnom: Globalna pretnja', 3, true, 2019, 'https://m.media-amazon.com/images/M/MV5BZTBmZGNiNGEtZmIxYy00NDgxLWE4MGItYzkwYjg2YzQ1Nzk1XkEyXkFqcGdeQXVyMDM2NDM2MQ@@._V1_UX182_CR0,0,182,268_AL_.jpg');

-- MOVIE_GENRE
INSERT INTO movie_genre(movie_id, genre_id) VALUES (100, 102);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (101, 101);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (101, 104);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (101, 102);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (102, 104);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (102, 102);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (102, 108);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (103, 101);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (103, 109);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (103, 107);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (104, 102);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (104, 110);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (105, 101);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (105, 104);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (105, 111);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (106, 105);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (106, 102);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (106, 108);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (107, 101);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (107, 104);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (107, 112);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (108, 102);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (108, 104);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (108, 105);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (109, 101);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (109, 102);
INSERT INTO movie_genre(movie_id, genre_id) VALUES (109, 111);

-- SEATS
INSERT INTO seat (seat_id, name) VALUES (101,'A1');
INSERT INTO seat (seat_id, name) VALUES (102,'A2');
INSERT INTO seat (seat_id, name) VALUES (103,'A3');
INSERT INTO seat (seat_id, name) VALUES (104,'A4');
INSERT INTO seat (seat_id, name) VALUES (105,'A5');
INSERT INTO seat (seat_id, name) VALUES (106,'B1');
INSERT INTO seat (seat_id, name) VALUES (107,'B2');
INSERT INTO seat (seat_id, name) VALUES (108,'B3');
INSERT INTO seat (seat_id, name) VALUES (109,'B4');
INSERT INTO seat (seat_id, name) VALUES (110,'B5');
INSERT INTO seat (seat_id, name) VALUES (111,'C1');
INSERT INTO seat (seat_id, name) VALUES (112,'C2');
INSERT INTO seat (seat_id, name) VALUES (113,'C3');
INSERT INTO seat (seat_id, name) VALUES (114,'C4');
INSERT INTO seat (seat_id, name) VALUES (115,'C5');
INSERT INTO seat (seat_id, name) VALUES (116,'D1');
INSERT INTO seat (seat_id, name) VALUES (117,'D2');
INSERT INTO seat (seat_id, name) VALUES (118,'D3');
INSERT INTO seat (seat_id, name) VALUES (119,'D4');
INSERT INTO seat (seat_id, name) VALUES (120,'D5');

-- USER (username == pass)
INSERT INTO user (user_id, language, firebase_token, name, username, password) VALUES (101, 'SERBIAN', '', 'Pera Peric', 'pera@gmail.com', '$2a$04$15E6BhAflHTE4bwqlXsA4uGi.QUKmJS77jMMK8aOwqQy60yj5d1sO');
INSERT INTO user (user_id, language, firebase_token, name, username, password) VALUES (102, 'SERBIAN', '', 'Sara Saric', 'sara@gmail.com', '$2a$04$15E6BhAflHTE4bwqlXsA4uGi.QUKmJS77jMMK8aOwqQy60yj5d1sO');
INSERT INTO user (user_id, language, firebase_token, name, username, password) VALUES (103, 'ENGLISH', '', 'John Doe', 'john@gmail.com', '$2a$04$15E6BhAflHTE4bwqlXsA4uGi.QUKmJS77jMMK8aOwqQy60yj5d1sO');
INSERT INTO user (user_id, language, firebase_token, name, username, password) VALUES (104, 'ENGLISH', '', 'Alice Alison', 'alice@gmail.com', '$2a$04$15E6BhAflHTE4bwqlXsA4uGi.QUKmJS77jMMK8aOwqQy60yj5d1sO');

-- COMMENT
INSERT INTO comment(comment_id, text, movie_id, user_id) VALUES (101, 'Komentar koji je pera dao za film Prevaranti', 100, 101);
INSERT INTO comment(comment_id, text, movie_id, user_id) VALUES (102, 'Komentar koji je sara dala za film Prevaranti', 100, 102);
INSERT INTO comment(comment_id, text, movie_id, user_id) VALUES (103, 'Comment which john made for movie Pokemon: Pikachu', 101, 103);
INSERT INTO comment(comment_id, text, movie_id, user_id) VALUES (104, 'Comment which Ben made for movie Pikachu detective', 101, 104);
INSERT INTO comment(comment_id, text, movie_id, user_id) VALUES (105, 'Komentar koji je pera dao za film John Wick', 103, 101);
INSERT INTO comment(comment_id, text, movie_id, user_id) VALUES (106, 'Komentar koji je sara dala za film John Wick', 103, 102);
INSERT INTO comment(comment_id, text, movie_id, user_id) VALUES (107, 'Comment which john made for movie Long shot', 104, 103);
INSERT INTO comment(comment_id, text, movie_id, user_id) VALUES (108, 'Comment which Ben made for movie Long shot', 104, 104);
INSERT INTO comment(comment_id, text, movie_id, user_id) VALUES (109, 'Komentar koji je pera dao za film Zavedi me ako mozes', 104, 101);

-- SCREENING
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (101, '2019-07-02 18:07:00', 1, 101);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (102, '2019-06-02 19:09:00', 2, 102);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (103, '2019-07-28 19:10:00', 3, 103);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (104, '2019-08-01 20:00:00', 4, 104);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (105, '2019-08-01 23:00:00', 1, 105);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (106, '2019-08-01 23:00:00', 2, 106);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (107, '2019-08-01 23:00:00', 3, 101);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (108, '2019-08-01 23:00:00', 4, 102);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (109, '2019-08-02 20:00:00', 1, 103);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (110, '2019-08-02 20:00:00', 2, 104);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (111, '2019-08-02 20:00:00', 3, 105);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (112, '2019-08-02 20:00:00', 4, 106);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (113, '2019-08-02 23:00:00', 1, 101);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (114, '2019-08-02 23:00:00', 2, 102);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (115, '2019-08-02 23:00:00', 3, 103);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (116, '2019-08-02 23:00:00', 4, 104);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (117, '2019-08-03 20:00:00', 1, 105);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (118, '2019-08-03 20:00:00', 2, 106);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (119, '2019-08-03 20:00:00', 3, 101);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (120, '2019-08-03 20:00:00', 4, 102);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (121, '2019-08-04 20:00:00', 1, 103);
INSERT INTO screening(screening_id, date, hall, movie_id) VALUES (122, '2019-08-04 20:00:00', 2, 104);

-- RESERVATION
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (101, 101, 101);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (102, 101, 102);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (103, 101, 103);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (104, 101, 104);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (105, 102, 101);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (106, 102, 102);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (107, 102, 103);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (108, 102, 104);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (109, 103, 101);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (110, 103, 102);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (111, 103, 103);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (112, 104, 104);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (113, 104, 101);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (114, 105, 102);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (115, 106, 103);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (116, 107, 104);
INSERT INTO reservation(reservation_id, screening_id, user_id) VALUES (117, 108, 101);

-- SEAT_RESERVATION
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(101, 101);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(102, 102);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(103, 103);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(104, 104);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(105, 105);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(106, 106);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(107, 107);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(108, 108);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(109, 109);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(110, 110);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(111, 111);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(112, 112);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(113, 113);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(114, 114);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(115, 115);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(116, 116);
INSERT INTO seat_reservation(reservation_id, seat_id) VALUES(117, 117);

-- RATINGS
INSERT INTO rating(rating_id, rate, movie_id, user_id) VALUES (101, 3, 100, 101);
INSERT INTO rating(rating_id, rate, movie_id, user_id) VALUES (102, 4, 100, 102);
INSERT INTO rating(rating_id, rate, movie_id, user_id) VALUES (103, 5, 101, 103);
INSERT INTO rating(rating_id, rate, movie_id, user_id) VALUES (104, 1, 101, 104);
INSERT INTO rating(rating_id, rate, movie_id, user_id) VALUES (105, 2, 103, 101);
INSERT INTO rating(rating_id, rate, movie_id, user_id) VALUES (106, 5, 103, 102);
INSERT INTO rating(rating_id, rate, movie_id, user_id) VALUES (107, 4, 104, 103);
INSERT INTO rating(rating_id, rate, movie_id, user_id) VALUES (108, 3, 104, 104);
INSERT INTO rating(rating_id, rate, movie_id, user_id) VALUES (109, 2, 104, 101);

package rs.ftn.skmd.cinemoj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class CinemojApplication {

	public static void main(String[] args) {
		SpringApplication.run(CinemojApplication.class, args);
	}

}

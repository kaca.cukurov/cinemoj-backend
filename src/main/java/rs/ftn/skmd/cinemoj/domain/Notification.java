package rs.ftn.skmd.cinemoj.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class Notification {

    /**
     * Represents Firebase token of a single device to whom notification should be sent.
     */
    private String token;

    private String title;

    private String body;
}

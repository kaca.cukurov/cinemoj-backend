package rs.ftn.skmd.cinemoj.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String nameSerbian;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String descriptionSerbian;

    @Column
    private Integer duration;

    @Column
    private Integer year;

    @Column
    private Integer rating;
    
    @Column
    private String pictureUrl;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "movie_genre",
            joinColumns = { @JoinColumn(name = "movie_id") },
            inverseJoinColumns = { @JoinColumn(name = "genre_id") })
    private List<Genre> genres;

    @OneToMany(mappedBy = "movie", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Comment> comments;

    @OneToMany(mappedBy = "movie", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Rating> ratings;

    @Column
    private boolean soon;
}

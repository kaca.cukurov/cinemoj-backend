package rs.ftn.skmd.cinemoj.utility;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import rs.ftn.skmd.cinemoj.domain.Notification;
import rs.ftn.skmd.cinemoj.web.dto.notification.*;

import java.lang.reflect.Type;

public class NotificationUtil {

    public static HttpEntity generateNotificationEntity(Notification notification, String firebaseServerKey) {

        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", "key=" + firebaseServerKey);
        httpHeaders.set("Content-Type", "application/json");

        final NotificationDto notificationDto = generateNotificationDto(notification);

        final Gson gson = new Gson();
        final Type type = new TypeToken<NotificationDto>() {
        }.getType();

        final String json = gson.toJson(notificationDto, type);

        return new HttpEntity<>(json, httpHeaders);
    }

    private static NotificationDto generateNotificationDto(Notification notification) {

        final NotificationData notificationData = new NotificationData();
        notificationData.setBody(notification.getBody());
        notificationData.setTitle(notification.getTitle());

        final NotificationDto notificationDto = new NotificationDto();
        notificationDto.setApnsData(new ApnsData(new Headers()));
        notificationDto.setAndroidData(new AndroidData());

        notificationDto.setData(notificationData);

        // To whom to send notification
        notificationDto.setTo(notification.getToken());

        return notificationDto;
    }

}

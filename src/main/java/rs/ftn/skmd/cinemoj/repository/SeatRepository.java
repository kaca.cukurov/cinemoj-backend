package rs.ftn.skmd.cinemoj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ftn.skmd.cinemoj.domain.Seat;

public interface SeatRepository extends JpaRepository<Seat, Long>{
}

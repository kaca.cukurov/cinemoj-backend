package rs.ftn.skmd.cinemoj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ftn.skmd.cinemoj.domain.Genre;

public interface GenreRepository extends JpaRepository<Genre, Long> {
}

package rs.ftn.skmd.cinemoj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ftn.skmd.cinemoj.domain.Screening;

public interface ScreeningRepository extends JpaRepository<Screening, Long>{
}

package rs.ftn.skmd.cinemoj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ftn.skmd.cinemoj.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}

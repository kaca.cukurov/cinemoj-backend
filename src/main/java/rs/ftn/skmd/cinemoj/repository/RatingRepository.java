package rs.ftn.skmd.cinemoj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.Rating;
import rs.ftn.skmd.cinemoj.domain.User;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Long>{

    List<Rating> findByUserAndMovie(User user, Movie movie);
}

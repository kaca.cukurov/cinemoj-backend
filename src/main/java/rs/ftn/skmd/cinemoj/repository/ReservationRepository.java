package rs.ftn.skmd.cinemoj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.domain.Screening;

import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> findByScreening(Screening screening);
}

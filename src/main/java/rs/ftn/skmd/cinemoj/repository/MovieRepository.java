package rs.ftn.skmd.cinemoj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import rs.ftn.skmd.cinemoj.domain.Movie;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long>{

    @Query("SELECT m FROM Movie m WHERE  m.soon = TRUE")
    List<Movie> findSoonMovies();
}

package rs.ftn.skmd.cinemoj.web.dto.notification;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class NotificationDto {

    /**
     * Firebase token of a device to which notification should be sent.
     */
    private String to;

    /**
     * Data to be sent {@link NotificationData} (call status).
     */
    private NotificationData data;

    /**
     * Used on iOS platform in order to handle background notifications.
     */
    @SerializedName("content_available")
    private boolean contentAvailable;

    /**
     * Platform-specific data, priority in this case, for iOS.
     */
    @SerializedName("apns")
    private ApnsData apnsData;

    /**
     * Platform-specific data, priority in this case, for Android.
     */
    @SerializedName("android")
    private AndroidData androidData;

    public NotificationDto() {
        this.contentAvailable = true;
    }
}

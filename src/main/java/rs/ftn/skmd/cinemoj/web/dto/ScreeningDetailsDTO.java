package rs.ftn.skmd.cinemoj.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ftn.skmd.cinemoj.domain.Screening;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ScreeningDetailsDTO {

    private Long id;
    private MovieDTO movieDTO;
    private String date;
    private String hall;

    public ScreeningDetailsDTO(Screening screening, String language){
        this.id = screening.getId();
        this.movieDTO = new MovieDTO(screening.getMovie(), language);
        this.date = screening.getDate() + "";
        this.hall = screening.getHall();
    }
}

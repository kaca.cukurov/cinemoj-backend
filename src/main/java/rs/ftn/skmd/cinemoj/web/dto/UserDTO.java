package rs.ftn.skmd.cinemoj.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ftn.skmd.cinemoj.domain.User;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private Long id;
    private String email;
    private String name;
    private String token;
    private String language;

    public UserDTO(User user, String token){
        this.email = user.getUsername();
        this.name = user.getName();
        this.token = token;
        this.id = user.getId();
        this.language = user.getLanguage();
    }
}

package rs.ftn.skmd.cinemoj.web.dto;

import lombok.*;
import rs.ftn.skmd.cinemoj.domain.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovieFeedbackDTO {

    @NonNull
    private Long user_id;
    @NonNull
    private Long reservation_id;
    private SoonMovieDetailsDTO movie;
    private Integer rate;
    private String text;
    private String hall;
    private String date;
    private String seats;

    public MovieFeedbackDTO(Comment comment, Rating rating, Reservation reservation, User user){
        this.user_id = user.getId();
        this.reservation_id = reservation.getId();
        this.movie = new SoonMovieDetailsDTO(reservation.getScreening().getMovie(), user.getLanguage());
        if(rating != null)
            this.rate = rating.getRate();
        if(comment != null)
            this.text = comment.getText();
        this.hall = reservation.getScreening().getHall();
        this.date = reservation.getScreening().getDate() + "";
        this.seats = "";
        reservation.getSeats().forEach(seat -> this.seats += seat.getName() + ", ");
        this.seats = this.seats.substring(0, this.seats.length()-2);
    }

}

package rs.ftn.skmd.cinemoj.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import rs.ftn.skmd.cinemoj.domain.*;
import rs.ftn.skmd.cinemoj.service.*;
import rs.ftn.skmd.cinemoj.web.dto.MovieFeedbackDTO;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/reservation")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private MovieService movieService;

    @GetMapping("/all")
    public ResponseEntity<List<MovieFeedbackDTO>> getMyReservations(){
        List<MovieFeedbackDTO> reservationDTOS = new ArrayList<>();
        User user = userService.getByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());
        List<Reservation> reservations = user.getReservations();
        for(Reservation res: reservations){
            reservationDTOS.add(getFeedbackDTO(user, res));
        }
        return new ResponseEntity<>(reservationDTOS, HttpStatus.OK);
    }

    @GetMapping("/{reservation_id}")
    public ResponseEntity<MovieFeedbackDTO> getMovieFeedback(@PathVariable Long reservation_id){
        User user = userService.getByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());
        Reservation reservation = this.reservationService.getById(reservation_id);
        return new ResponseEntity<>(getFeedbackDTO(user, reservation), HttpStatus.OK);
    }

    @PostMapping("/saveFeedback")
    public ResponseEntity saveFeedback(@RequestBody @Valid MovieFeedbackDTO feedbackDTO){
        User user = this.userService.getById(feedbackDTO.getUser_id());
        final Reservation reservation = this.reservationService.getById(feedbackDTO.getReservation_id());
        final Movie movie =reservation.getScreening().getMovie();
        Comment comment = new Comment();
        comment.setMovie(movie);
        comment.setUser(user);
        comment.setText(feedbackDTO.getText());
        this.commentService.save(comment);
        movie.getComments().add(comment);
        user.getComments().add(comment);

        Rating rating = new Rating();
        rating.setMovie(movie);
        rating.setUser(user);
        rating.setRate(feedbackDTO.getRate());
        this.ratingService.save(rating);
        movie.getRatings().add(rating);
        user.getRatings().add(rating);

        this.movieService.save(movie);
        this.userService.save(user);
        changeRate(movie);
        return new ResponseEntity(HttpStatus.OK);
    }

    private MovieFeedbackDTO getFeedbackDTO(User user, Reservation reservation){
        Comment comment = this.commentService.findByUserAndMovie(user, reservation.getScreening().getMovie());
        Rating rating = this.ratingService.findByUserAndMovie(user, reservation.getScreening().getMovie());
        return new MovieFeedbackDTO(comment, rating, reservation, user);
    }

    private void changeRate(Movie movie){
        List<Rating> ratings = movie.getRatings();
        Integer allRatings = 0;
        for(Rating r: ratings){
            allRatings += r.getRate();
        }
        movie.setRating(allRatings / ratings.size());
        this.movieService.save(movie);
    }
}

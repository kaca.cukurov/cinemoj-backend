package rs.ftn.skmd.cinemoj.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.service.MovieService;
import rs.ftn.skmd.cinemoj.service.UserService;
import rs.ftn.skmd.cinemoj.web.dto.MovieDTO;
import rs.ftn.skmd.cinemoj.web.dto.SoonDTO;
import rs.ftn.skmd.cinemoj.web.dto.SoonMovieDetailsDTO;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/movie")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @Autowired
    private UserService userService;

    @GetMapping("/soon")
    public ResponseEntity<List<MovieDTO>> getSoonMovies(){
        User user = userService.getByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());
        List<MovieDTO> soonDTOS = new ArrayList<>();
        List<Movie> movies = this.movieService.getAllSoon();
        movies.forEach(movie -> soonDTOS.add(new MovieDTO(movie, user.getLanguage())));
        return new ResponseEntity<>(soonDTOS, HttpStatus.OK);
    }

    @GetMapping("/soon/{id}")
    public ResponseEntity<SoonMovieDetailsDTO> getSoonMovieDetailsById(@PathVariable Long id){
        User user = userService.getByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());
        Movie movie = this.movieService.getById(id);
        return new ResponseEntity<>(new SoonMovieDetailsDTO(movie, user.getLanguage()), HttpStatus.OK);
    }
}

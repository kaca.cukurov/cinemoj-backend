package rs.ftn.skmd.cinemoj.web.dto.notification;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ApnsData {

    @SerializedName("headers")
    private Headers headers;

}

package rs.ftn.skmd.cinemoj.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ftn.skmd.cinemoj.domain.Comment;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.service.CommentService;
import rs.ftn.skmd.cinemoj.service.MovieService;
import rs.ftn.skmd.cinemoj.service.RatingService;
import rs.ftn.skmd.cinemoj.web.dto.CommentDTO;
import rs.ftn.skmd.cinemoj.web.dto.MovieFeedbackDTO;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private MovieService movieService;

    @Autowired
    private RatingService ratingService;

    @GetMapping("/all/{movie_id}")
    public ResponseEntity<List<CommentDTO>> getMovieComments(@PathVariable Long movie_id){
        Movie movie = this.movieService.getById(movie_id);
        List<Comment> comments = this.commentService.findByMovie(movie);
        List<CommentDTO> commentDTOS = new ArrayList<>();
        comments.forEach(comment -> commentDTOS.add(new CommentDTO(comment,
                this.ratingService.findByUserAndMovie(comment.getUser(), comment.getMovie()))));
        return new ResponseEntity<>(commentDTOS, HttpStatus.OK);
    }
}

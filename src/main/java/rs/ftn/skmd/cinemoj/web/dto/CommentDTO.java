package rs.ftn.skmd.cinemoj.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ftn.skmd.cinemoj.domain.Comment;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.Rating;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommentDTO {

    private Long id;
    private String text;
    private String username;
    private Integer rate;

    public CommentDTO(Comment comment, Rating rating){
        this.id = comment.getId();
        this.text = comment.getText();
        this.username = comment.getUser().getUsername();
        this.rate = rating.getRate();
    }

    public CommentDTO(Comment comment){
        this.id = comment.getId();
        this.text = comment.getText();
        this.username = comment.getUser().getUsername();
    }
}

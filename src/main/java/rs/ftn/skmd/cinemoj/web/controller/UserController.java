package rs.ftn.skmd.cinemoj.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.security.JWTUtils;
import rs.ftn.skmd.cinemoj.service.UserDetailsServiceImpl;
import rs.ftn.skmd.cinemoj.service.UserService;
import rs.ftn.skmd.cinemoj.web.dto.FirebaseTokenDTO;
import rs.ftn.skmd.cinemoj.web.dto.UserCredentialsDTO;
import rs.ftn.skmd.cinemoj.web.dto.UserDTO;
import rs.ftn.skmd.cinemoj.web.dto.UserRegistrationDTO;

import java.util.ArrayList;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private JWTUtils jwtUtils;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody UserCredentialsDTO userCredentialsDTO){
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    userCredentialsDTO.getEmail(), userCredentialsDTO.getPassword());
            authenticationManager.authenticate(token);

            User user = this.userService.getByUsername(userCredentialsDTO.getEmail());
            UserDetails details = userDetailsService.loadUserByUsername(user.getUsername());
            UserDTO userDTO = new UserDTO(user, jwtUtils.generateToken(details));

            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/register")
    public ResponseEntity registration(@RequestBody UserRegistrationDTO registrationDTO){
        User user = this.userService.getByUsername(registrationDTO.getEmail());
        if(user != null)
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
        String pass = bc.encode(registrationDTO.getPassword());
        user = new User();
        user.setName(registrationDTO.getName());
        user.setUsername(registrationDTO.getEmail());
        user.setPassword(pass);
        user.setComments(new ArrayList<>());
        user.setRatings(new ArrayList<>());
        user.setReservations(new ArrayList<>());
        user.setLanguage("ENGLISH");

        this.userService.save(user);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/language/{lang}")
    public ResponseEntity changeLanguage(@PathVariable String lang){
        User user = userService.getByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        user.setLanguage(lang);
        this.userService.save(user);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/firebaseRegistrationToken")
    public ResponseEntity setToken(@RequestBody FirebaseTokenDTO firebaseTokenDTO){
        User user = userService.getByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        user.setFirebaseToken(firebaseTokenDTO.getToken());
        this.userService.save(user);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/logout")
    public ResponseEntity deleteToken() {
        User user = userService.getByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        user.setFirebaseToken("");
        this.userService.save(user);
        return new ResponseEntity(HttpStatus.OK);
    }
}

package rs.ftn.skmd.cinemoj.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ftn.skmd.cinemoj.domain.Movie;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SoonDTO {

    private Long id;
    private String name;
    private String pictureUrl;

    public SoonDTO(Movie movie, String language){
        this.id = movie.getId();
        this.pictureUrl=movie.getPictureUrl();
        if(language.equals("SERBIAN")){
            this.name = movie.getNameSerbian();
        }else{
            this.name = movie.getName();
        }
    }
}

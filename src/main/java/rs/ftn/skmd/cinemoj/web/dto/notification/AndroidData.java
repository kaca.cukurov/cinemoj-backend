package rs.ftn.skmd.cinemoj.web.dto.notification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class AndroidData {

    /**
     * Priority of a notification defining the way Firebase handles notification.
     */
    private String priority;

    public AndroidData() {
        this.priority = "high";
    }
}

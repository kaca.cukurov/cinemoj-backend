package rs.ftn.skmd.cinemoj.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.domain.Seat;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.service.ReservationService;
import rs.ftn.skmd.cinemoj.service.ScreeningService;
import rs.ftn.skmd.cinemoj.service.SeatService;
import rs.ftn.skmd.cinemoj.service.UserService;
import rs.ftn.skmd.cinemoj.web.dto.ReservationDTO;
import rs.ftn.skmd.cinemoj.web.dto.ScreeningDetailsDTO;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/screening")
public class ScreeningController {

    @Autowired
    private ScreeningService screeningService;

    @Autowired
    private UserService userService;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private SeatService seatService;

    @GetMapping("/all")
    public ResponseEntity<List<ScreeningDetailsDTO>> getAllScreenings(){
        User user = userService.getByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        List<ScreeningDetailsDTO> screeningDTOS = new ArrayList<>();
        List<Screening> screenings = this.screeningService.getAll();
        screenings.forEach(screening -> screeningDTOS.add(new ScreeningDetailsDTO(screening, user.getLanguage())));
        return new ResponseEntity<>(screeningDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ScreeningDetailsDTO> getScreeningDetails(@PathVariable Long id){
        User user = userService.getByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName());
        Screening screening = this.screeningService.getById(id);
        return new ResponseEntity<>(new ScreeningDetailsDTO(screening, user.getLanguage()), HttpStatus.OK);
    }

    @PostMapping("/reserve")
    public ResponseEntity makeReservation(@RequestBody ReservationDTO reservationDTO){
        User user = userService.getByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        Screening screening = this.screeningService.getById(reservationDTO.getScreening_id());
        List<Reservation> reservations = this.reservationService.findByScreening(screening);
        List <Seat> freeSeats = getFreeSeats(reservations);
        if(freeSeats.size() < reservationDTO.getTicketsNumber()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        Reservation newRes = new Reservation();
        newRes.setSeats(new ArrayList<>());
        newRes.setScreening(screening);
        newRes.setUser(user);
        for(int i = 0; i < reservationDTO.getTicketsNumber(); i++){
            newRes.getSeats().add(freeSeats.get(i));
        }
        this.reservationService.save(newRes);
        user.getReservations().add(newRes);
        this.userService.save(user);
        screening.getReservations().add(newRes);
        this.screeningService.save(screening);

        for(int i = 0; i < newRes.getSeats().size(); i++){
            newRes.getSeats().get(i).getReservations().add(newRes);
            this.seatService.save(newRes.getSeats().get(i));
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    private List<Seat> getFreeSeats(List<Reservation> reservations){
        List<Seat> allSeats = this.seatService.getAll();
        List<Seat> occupiedSeats = new ArrayList<>();
        reservations.forEach(reservation -> occupiedSeats.addAll(reservation.getSeats()));
        List<Seat> freeSeats = new ArrayList<>();
        for(Seat seat: allSeats){
            if(!occupiedSeats.contains(seat))
                freeSeats.add(seat);
        }
        return freeSeats;
    }
}

package rs.ftn.skmd.cinemoj.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import rs.ftn.skmd.cinemoj.domain.Movie;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SoonMovieDetailsDTO {

    private Long id;
    private String name;
    private String description;
    private Integer duration;
    private Integer year;
    private Integer rating;
    private String genres;
    private String pictureUrl;

    public SoonMovieDetailsDTO(Movie movie, String language){
        this.id = movie.getId();
        this.genres = "";
        if(language.equals("SERBIAN")){
            this.name = movie.getNameSerbian();
            this.description = movie.getDescriptionSerbian();
            movie.getGenres().forEach(genre -> this.genres += genre.getNameSerbian() + ", ");
        }else{
            this.name = movie.getName();
            this.description = movie.getDescription();
            movie.getGenres().forEach(genre -> this.genres += genre.getName() + ", ");
        }
        this.genres = this.genres.substring(0, this.genres.length()-2);
        this.duration = movie.getDuration();
        this.year = movie.getYear();
        this.rating = movie.getRating();
        this.pictureUrl = movie.getPictureUrl();
    }
}

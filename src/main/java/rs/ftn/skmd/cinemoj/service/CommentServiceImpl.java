package rs.ftn.skmd.cinemoj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ftn.skmd.cinemoj.domain.Comment;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.repository.CommentRepository;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public List<Comment> getAll() {
        return this.commentRepository.findAll();
    }

    @Override
    public void save(Comment comment) {
        this.commentRepository.save(comment);
    }

    @Override
    public Comment findByUserAndMovie(User user, Movie movie) {
        final List<Comment> comments = this.commentRepository.findByUserAndMovie(user, movie);
        if (comments != null && comments.size() > 0) {
            return this.commentRepository.findByUserAndMovie(user, movie).get(0);
        }
        return null;
    }

    @Override
    public List<Comment> findByMovie(Movie movie){
        return this.commentRepository.findByMovie(movie);
    }
}

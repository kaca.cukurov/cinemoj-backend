package rs.ftn.skmd.cinemoj.service;

import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.domain.Screening;

import java.util.List;

public interface ReservationService {

    void save(Reservation reservation);

    Reservation getById(Long id);

    List<Reservation> getAll();

    List<Reservation> findByScreening(Screening screening);
}

package rs.ftn.skmd.cinemoj.service;

import rs.ftn.skmd.cinemoj.domain.Genre;

import java.util.List;

public interface GenreService {

    List<Genre> getAll();
}

package rs.ftn.skmd.cinemoj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import rs.ftn.skmd.cinemoj.domain.Notification;
import rs.ftn.skmd.cinemoj.utility.NotificationUtil;

@Service
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private Environment environment;

    public void sendNotification(Notification notification) {

        final String firebaseServerKey = environment.getProperty("firebase-server-key");
        final String firebaseApiUrl = environment.getProperty("firebase-api-url");

        final RestTemplate restTemplate = new RestTemplate();
        final HttpEntity httpEntity = NotificationUtil.generateNotificationEntity(notification, firebaseServerKey);

        final String response = restTemplate.postForObject(firebaseApiUrl, httpEntity, String.class);
        System.out.println("[INFO]: Firebase response: " + response);
    }

}
package rs.ftn.skmd.cinemoj.service;

import rs.ftn.skmd.cinemoj.domain.Screening;

import java.util.List;

public interface ScreeningService {

    void save(Screening screening);

    Screening getById(Long id);

    List<Screening> getAll();
}

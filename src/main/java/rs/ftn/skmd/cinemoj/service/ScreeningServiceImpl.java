package rs.ftn.skmd.cinemoj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.repository.ScreeningRepository;

import java.util.List;

@Service
public class ScreeningServiceImpl implements ScreeningService {

    @Autowired
    private ScreeningRepository screeningRepository;

    @Override
    public void save(Screening screening){
        this.screeningRepository.save(screening);
    }

    @Override
    public Screening getById(Long id){
        return this.screeningRepository.getOne(id);
    }

    @Override
    public List<Screening> getAll(){
        return this.screeningRepository.findAll();
    }
}

package rs.ftn.skmd.cinemoj.service;

import rs.ftn.skmd.cinemoj.domain.Notification;

public interface NotificationService {

    void sendNotification(Notification notification);
}

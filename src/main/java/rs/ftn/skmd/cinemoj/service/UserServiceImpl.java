package rs.ftn.skmd.cinemoj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.repository.UserRepository;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(User user){
        this.userRepository.save(user);
    }

    @Override
    public User getById(Long id){
        return this.userRepository.getOne(id);
    }

    @Override
    public List<User> getAll(){
        return this.userRepository.findAll();
    }

    @Override
    public User getByUsername(String username){
        return this.userRepository.findByUsername(username);
    }
}

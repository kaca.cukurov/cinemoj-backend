package rs.ftn.skmd.cinemoj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ftn.skmd.cinemoj.domain.Reservation;
import rs.ftn.skmd.cinemoj.domain.Screening;
import rs.ftn.skmd.cinemoj.repository.ReservationRepository;

import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    @Override
    public void save(Reservation reservation){
        this.reservationRepository.save(reservation);
    }

    @Override
    public Reservation getById(Long id){
        return this.reservationRepository.getOne(id);
    }

    @Override
    public List<Reservation> getAll(){
        return this.reservationRepository.findAll();
    }

    @Override
    public List<Reservation> findByScreening(Screening screening){
        return this.reservationRepository.findByScreening(screening);
    }
}

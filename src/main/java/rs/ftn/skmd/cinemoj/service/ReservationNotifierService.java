package rs.ftn.skmd.cinemoj.service;

import ch.qos.logback.core.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import rs.ftn.skmd.cinemoj.domain.Notification;
import rs.ftn.skmd.cinemoj.repository.ScreeningRepository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class ReservationNotifierService {

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private NotificationService notificationService;

    @Scheduled(fixedRate = 60000)
    public void sendNotifications() {

        LocalDateTime localDateTime = LocalDateTime.now().plusHours(1);
        localDateTime = localDateTime.withSecond(0).withNano(0);
        LocalDateTime finalLocalDateTime = localDateTime;

        reservationService.getAll().forEach(reservation -> {

            LocalDateTime reservationDate = reservation.getScreening().getDate()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime();

            if (reservationDate.isEqual(finalLocalDateTime)) {
                if (reservation.getUser().getFirebaseToken() == null || reservation.getUser().getFirebaseToken().equals("")) return;
                System.out.println("SENDING NOTIFICATION to " + reservation.getUser().getUsername());

                boolean isEnglishSet = reservation.getUser().getLanguage().equals("ENGLISH");
                String title = isEnglishSet ? "Reservation reminder" : "Rezervacija";
                String body = isEnglishSet ? "Screening " + reservation.getScreening().getMovie().getName() +
                        " will start in an hour in hall " + reservation.getScreening().getHall() :
                        "Projekcija " + reservation.getScreening().getMovie().getNameSerbian() +
                                " počinje za sat vremena u sali " + reservation.getScreening().getHall();

                Notification notification = new Notification(reservation.getUser().getFirebaseToken(), title,
                        body);
                notificationService.sendNotification(notification);
            }
        });
    }
}

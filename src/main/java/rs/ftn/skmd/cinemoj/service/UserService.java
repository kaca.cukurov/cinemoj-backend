package rs.ftn.skmd.cinemoj.service;

import rs.ftn.skmd.cinemoj.domain.User;

import java.util.List;

public interface UserService {

    void save(User user);

    User getById(Long id);

    List<User> getAll();

    User getByUsername(String username);
}

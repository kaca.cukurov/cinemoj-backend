package rs.ftn.skmd.cinemoj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.repository.MovieRepository;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public void save(Movie movie){
        this.movieRepository.save(movie);
    }

    @Override
    public Movie getById(Long id){
        return this.movieRepository.findById(id).get();
    }

    @Override
    public List<Movie> getAll(){
        return this.movieRepository.findAll();
    }

    @Override
    public List<Movie> getAllSoon(){ return this.movieRepository.findSoonMovies(); }
}

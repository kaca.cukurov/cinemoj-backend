package rs.ftn.skmd.cinemoj.service;

import rs.ftn.skmd.cinemoj.domain.Seat;

import java.util.List;

public interface SeatService {

    void save(Seat seat);

    Seat getById(Long id);

    List<Seat> getAll();
}

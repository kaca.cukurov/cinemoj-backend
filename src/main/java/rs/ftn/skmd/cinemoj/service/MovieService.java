package rs.ftn.skmd.cinemoj.service;

import rs.ftn.skmd.cinemoj.domain.Movie;

import java.util.List;

public interface MovieService {

    void save(Movie movie);

    Movie getById(Long id);

    List<Movie> getAll();

    List<Movie> getAllSoon();
}

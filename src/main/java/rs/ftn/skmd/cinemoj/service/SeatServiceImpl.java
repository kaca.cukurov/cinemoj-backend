package rs.ftn.skmd.cinemoj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ftn.skmd.cinemoj.domain.Seat;
import rs.ftn.skmd.cinemoj.repository.SeatRepository;

import java.util.List;

@Service
public class SeatServiceImpl implements SeatService {

    @Autowired
    private SeatRepository seatRepository;

    @Override
    public void save(Seat seat){
        this.seatRepository.save(seat);
    }

    @Override
    public Seat getById(Long id){
        return this.seatRepository.getOne(id);
    }

    @Override
    public List<Seat> getAll(){
        return this.seatRepository.findAll();
    }
}

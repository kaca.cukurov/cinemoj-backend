package rs.ftn.skmd.cinemoj.service;

import rs.ftn.skmd.cinemoj.domain.Comment;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.User;

import java.util.List;

public interface CommentService {

    List<Comment> getAll();

    void save(Comment comment);

    Comment findByUserAndMovie(User user, Movie movie);

    List<Comment> findByMovie(Movie movie);
}

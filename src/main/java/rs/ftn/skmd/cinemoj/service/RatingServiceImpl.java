package rs.ftn.skmd.cinemoj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.Rating;
import rs.ftn.skmd.cinemoj.domain.User;
import rs.ftn.skmd.cinemoj.repository.RatingRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {

    @Autowired
    private RatingRepository ratingRepository;

    @Override
    public void save (Rating rating){
        this.ratingRepository.save(rating);
    }

    @Override
    public Rating getById(Long id){
        return this.ratingRepository.getOne(id);
    }

    @Override
    public List<Rating> getAll(){
        return this.ratingRepository.findAll();
    }

    @Override
    public Rating findByUserAndMovie(User user, Movie movie){
        final List<Rating> ratings = this.ratingRepository.findByUserAndMovie(user, movie);
        if(ratings!=null && ratings.size()>0){
            return this.ratingRepository.findByUserAndMovie(user, movie).get(0);
        }
        return null;
    }
}

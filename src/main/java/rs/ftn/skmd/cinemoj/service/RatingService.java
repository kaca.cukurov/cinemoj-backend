package rs.ftn.skmd.cinemoj.service;

import rs.ftn.skmd.cinemoj.domain.Movie;
import rs.ftn.skmd.cinemoj.domain.Rating;
import rs.ftn.skmd.cinemoj.domain.User;

import java.util.List;

public interface RatingService {

    void save (Rating rating);

    Rating getById(Long id);

    List<Rating> getAll();

    Rating findByUserAndMovie(User user, Movie movie);
}
